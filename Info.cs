namespace ExemploJSON
{
    class Info
    {
        public int ActualPage { get; set; }
        public int TotalPage { get; set; }
    }
}