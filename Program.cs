﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json.Linq;

namespace ExemploJSON
{
    class Program
    {
        static void Main(string[] args)
        {
            JObject json = JObject.Parse(File.ReadAllText(@"exemplo.json"));
            
            JArray array = (JArray)json["Pessoas"];
            JObject obj = (JObject)json["Info"];
            
            List<Pessoa> pessoas = array.ToObject<List<Pessoa>>();
            Info pageInfo = obj.ToObject<Info>();

            Console.WriteLine(string.Format("Página {0} de {1}", pageInfo.ActualPage, pageInfo.TotalPage));

            foreach (Pessoa pessoa in pessoas)
            {
                Console.WriteLine(string.Format("{0} - {1}", pessoa.Nome, pessoa.Nascimento.ToShortDateString()));
            }
        }
    }
}
